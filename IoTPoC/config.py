DEVICE_ID="237BF81C4D"
ENDPOINT = "a7v7nyeevavqv-ats.iot.us-east-2.amazonaws.com"
PORT = 8883
CLIENT_ID = "raspberryPi"
CERT_ID="52b0251caf"
PATH_TO_CERT = "../certs/52b0251caf-certificate.pem.crt"
PATH_TO_KEY = "../certs/52b0251caf-private.pem.key"
PATH_TO_ROOT = "../certs/root.pem"
MESSAGE = "Hello World"
TOPIC_IDENTIFY = "okay/identify"
TOPIC_IDENTIFY_RES = "okay/identify/response"
TOPIC_LINKING= "okay/linking"
TOPIC_LINKING_RES = "okay/linking/response"
TOPIC_DEVICE_UNIQUE="okay/things/237BF81C4D"
TOPIC_LINKING_PATTERN="okay/things/237BF81C4D/pattern"
INIT_MESSAGE= {
    "DEVICE": DEVICE_ID,
    "CERT": CERT_ID
}
LINKING_MESSAGE= {
    "DEVICE": DEVICE_ID,
    "STATUS": "ACTIVE"
}
BUTTON = 10
LED = 8