import RPi.GPIO as GPIO
from time import sleep
import config
import mqtt

def button_callback(channel):
    GPIO.output(config.LED, GPIO.HIGH)
    print("Button was pushed!")
    mqtt.publish(config.TOPIC_IDENTIFY, config.INIT_MESSAGE, 1)
    mqtt.subscribeTo(config.TOPIC_IDENTIFY_RES)
    sleep(1)
    GPIO.output(config.LED, GPIO.LOW)
    sleep(1)

def setup():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(config.BUTTON, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.setup(config.LED, GPIO.OUT, initial=GPIO.LOW)
    GPIO.add_event_detect(config.BUTTON,GPIO.RISING,callback=button_callback)
    message = input("Press enter to quit\n\n")
    GPIO.cleanup()

def diodeBlink(pattern):
    for time in pattern:
        GPIO.output(config.LED, GPIO.HIGH)
        sleep(time)
        GPIO.output(config.LED, GPIO.LOW)
        sleep(0.2)