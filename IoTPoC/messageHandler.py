import config
import json
import mqtt
import pairing

def onMessage(client, userdata, message):
    msg = json.loads(message.payload)

    if msg["type"]=="identify":
        onIdentify(msg)
    elif msg["type"]=="pattern":
        onPattern(msg)
    else:
        print("Unrecognized request")


def onIdentify(message):
    print("Device verified!!")
    newCredentials = True
    #logic for downloading and setting new keys
    try:
        if newCredentials:
            mqtt.publish(config.TOPIC_DEVICE_UNIQUE, config.LINKING_MESSAGE, 0)
            mqtt.subscribeTo(config.TOPIC_LINKING_PATTERN)
    except:
        print("An exception occurred")

def onPattern(message):
    blinkingPattern = message["pattern"]
    pairing.diodeBlink(blinkingPattern)


