import time as t
import json
import AWSIoTPythonSDK.MQTTLib as AWSIoTPyMQTT
import config
import messageHandler
import pairing

myAWSIoTMQTTClient = AWSIoTPyMQTT.AWSIoTMQTTClient(config.CLIENT_ID)

def connect():
    myAWSIoTMQTTClient.configureEndpoint(config.ENDPOINT, config.PORT)
    myAWSIoTMQTTClient.configureCredentials(config.PATH_TO_ROOT, config.PATH_TO_KEY, config.PATH_TO_CERT)
    myAWSIoTMQTTClient.connect()
    print("MQTT connection established!")
    return True

def disconnect():
    myAWSIoTMQTTClient.disconnect()
    print("disconnected!!")

def subscribeTo(topic):
    myAWSIoTMQTTClient.subscribe(topic, 0, messageHandler.onMessage)

def publish(topic, msg, RANGE):
    for i in range (RANGE):
        myAWSIoTMQTTClient.publish(topic, json.dumps(msg), 1)
        print("Published: " + json.dumps(msg) + "' to the topic: " + topic)

def unsubscribe(topic):
    myAWSIoTMQTTClient.unsubscribe(topic)