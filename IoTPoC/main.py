import pairing
import config
import mqtt

def init():
    mqttConnected = mqtt.connect()
    if mqttConnected:
        pairing.setup()

if __name__=='__main__':
    init()